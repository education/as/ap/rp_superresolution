import ipywidgets as widgets

from show_movies_anim import anim

resourceDir = 'c:/rp/'
moviesDir = resourceDir + 'Movies/'

image = widgets.Dropdown(
    options=['image1', 'image2'],
    value='image1',
    description='Image:',
#    style = style
)
strength = widgets.Dropdown(
    options=['50', '80', '100'],
    value='50',
    description='Strength:'
)
density = widgets.Dropdown(
    options=['20', '30', '50'],
    value='20',
    description='Density:'
)
psf = widgets.Dropdown(
    options=['10', '20', '30'],
    value='10',
    description='PSF:'
)
frames = widgets.IntSlider(
    min=1,
    max=2500,
    value=1250,
    step=1,
    description='# frames:'
)
fps = widgets.IntSlider(
    min=1,
    max=10,
    value=5,
    step=1,
    description='FPS:',
)
button = widgets.Button(
    description="Start movie!"
)

ui=widgets.VBox([image, strength, density, psf, frames, fps, button])

def callAnim(b):
    global image, strength, density, psf
    global frames, fps
    fname = '%s%s_st%s_den%s_psf%s.ics'%(moviesDir, image.value, strength.value, density.value, psf.value)
    anim(fname, frames.value, fps.value)
    
button.on_click(callAnim)
