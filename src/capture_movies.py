import numpy as np
import cv2
import datetime as dt
import diplib as dip

# font for monitoring mode and reporting
font = cv2.FONT_HERSHEY_PLAIN
fontSize = 2
fontColor = (0, 0, 255)

# initialise video capture
cap = cv2.VideoCapture(0)

def capture(imName='capture', x=40, y=30):
    print('Capture output of first camera device.')
    print('Display RGB with info in window (see framerate after [fps])')
    print('If choosen all frames are stored as gray-values in a buffer (see buffersize after [frames])')
    print('Change behavious with key commands while window has focus')
    print('Key commands in Capture window:')
    print('  [r]ecord: store frames in buffer')
    print('  [p]ause: don\'t store frames')
    print('  [c]lear: clear buffer')
    print('  [s]ave: write buffer as ICS and clear buffer')
    print('  [q]uit: clear buffer and stop')

    # array (used as ring) for running average
    ring = np.full((10), 0)
    ringIndex = 0

    # initialisation
    image = []
    rec = False
    bufferCount = 0
    lastFrame = dt.datetime.now()

    # window for showing capture
    winName = 'Capture'
    cv2.namedWindow(winName)        # Create a named window
    cv2.moveWindow(winName, x, y)  # Move it to (40,30)
    
    while(cap.isOpened()):

        # Capture frame-by-frame
        ret, frame = cap.read()

        if ret == True:             

            # calculate the framerate (with running average)
            now = dt.datetime.now()       
            diffInterval = now - lastFrame
            frameRate = 1/diffInterval.total_seconds()
            ring[ringIndex] = frameRate
            ringIndex = (ringIndex + 1) % len(ring)
            frameRate = np.average(ring)
            lastFrame = now

            # frame is buffered
            if rec:
                # Our operations on the frame come here
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                # buffer the frame
                image.append(gray)
                #out.write(frame)
                bufferCount += 1

            if rec:
                # Add record-mark in frame
                cv2.circle(frame, (620, 460), 20, (0,0,255), -1)
            cv2.putText(frame, 'fps: %.1f'%(frameRate), (20, 20), font, 2, (0,0,255))
            cv2.putText(frame, 'frames: %d'%(bufferCount), (20, 40), font, 2, (0,0,255))

            # Display the resulting frame
            cv2.imshow(winName, frame)
            # Activate display and catch key
            k = cv2.waitKey(1)
            if k==ord('q'):
                image = []
                break
            elif k==ord('r'):
                rec = True
            elif k==ord('p'):
                rec = False
            elif k==ord('c'):
                image = []
                bufferCount = 0
            elif k==ord('s'):
                rec = False
                if len(image) > 0:
                    now = dt.datetime.now()
                    fname = now.strftime("%Y%m%d_%H%M%S") + '_' + imName
                    cv2.putText(frame, 'Saving: '+fname+'...', (20 , 460), font, 2, (0,0,255))
                    cv2.imshow(winName, frame)
                    cv2.waitKey(1)
                    dip.ImageWriteICS(dip.Image(np.array(image)), fname)
                else:
                    cv2.putText(frame, 'Not saving empty buffer!', (20, 460), font, 2, (0,0,255))
                    cv2.imshow(winName, frame)
                    cv2.waitKey(2000)
                image = []
                bufferCount = 0
        else:
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    # execute only if run as a script
    capture()