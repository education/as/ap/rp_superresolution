import numpy as np

import matplotlib as mpl
mpl.use('Qt5Agg')

import matplotlib.pyplot as plt
import matplotlib.animation as animation

import diplib as dip

import argparse

import sys

from progress.bar import Bar

dipDir = 'c:/rp/diplib/'

fnameDefault = dipDir + 'images/chromo3d.ics'
framesDefault = 16 # number of frames to show
speedDefault = 1 # frames per second

def handle_close(evt):
    print('Closed animation!')

def anim(fname, frames, speed):
    if not isinstance(frames, int) or frames <= 0:
        print("ERROR: frames ({f}) must be an integer > 0".format(f=frames))
        sys.exit()
    if not isinstance(speed, int) or speed <=0:
        print("ERROR: speed ({s}) must be an integer > 0".format(s=speed))
        sys.exit()
    try:
        open(fname, 'rb')
    except OSError:
        print("ERROR: file ({fn}) could not open/read file".format(fn=fname))
        sys.exit()

    duration = frames/speed # duration of move in seconds
    interval = 1/speed*1000 # interval between frames in milliseconds

    print('Showing %d frames in %2.1f seconds'%(frames, duration))
    print('Using file %s'%(fname))
    img = dip.ImageReadICS(fname)
    sz = img.Sizes()
    print('  Image size: %d x %d'%(sz[0],sz[1]))
    imgFrames = sz[2] # number of frames in image
    print('  Number of frames: %d'%imgFrames)

    fig = plt.figure() #figsize = tuple(sz[:2])
    fig.canvas.mpl_connect('close_event', handle_close)

    mng = plt.get_current_fig_manager()
    # mng.window.wm_geometry("1280x1024+1680+0") # mpl.use('TkAgg')
    mng.window.setGeometry(1680, 0, 1280, 1024)
    #mng.window.state('zoomed')

    plt.ion() # non-blocking show()

    plt.axis('off')
    
    movie = []

    print('Randomizing frames...')
    indx = list(range(((frames-1)//imgFrames+1)*imgFrames))
    indx = np.mod(indx, imgFrames)
    np.random.shuffle(indx)
    indx = indx[:frames]

    bar = Bar('Preparing movie', max=len(indx))
    for i in indx:
        bar.next()
        slide = img[:,:,int(i)].Squeeze()
        movie.append([plt.imshow(slide, animated=True)])
    bar.finish()

    ani = animation.ArtistAnimation(fig, movie, interval=interval, repeat=False, blit=False)

    print('Showing movie... [wait approximately %d seconds before movie starts]'%int(0.1*duration))
    plt.show()
    plt.pause(duration*1.1) # pause execution for duration of movie plus extra 10% for initialisation of figure
    plt.close('all')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Play ics movie")
    parser.add_argument("fname", nargs="?", default=fnameDefault, help="filename of the movie [default {fn}]".format(fn=fnameDefault))
    parser.add_argument("-f", "--frames", type=int, action="store", default=framesDefault, help="number of frames [default {f}]".format(f=framesDefault))
    parser.add_argument("-s", "--speed", type=int, action="store", default=speedDefault, help="speed in frames per second (fps) [default {s}]".format(s=speedDefault))
    args = parser.parse_args()
    anim(args.fname, args.frames, args.speed)