# Research Practicum Superresolution

First read and follow these instructions carefully! After you finished setting up everything for the practicum as described below you can start reading the `research_practicum_manual` which is found in the `doc` directory. All python scripts needed for this practicum are found in the `src` directory.

## Introduction

This instruction explains how to prepare for the **Superresolution Research Practicum** (RP). It expects you to login on the computer provided for this RP with your netid and use `PowerShell` to give the right commands. All commands and further instructions are given and will lead you to a working `Jupyter Notebook` to work on the RP.

## Initial setup

These instructions only have to be executed once to prepare for the RP.

The administrator of the computer has to allow for users to create a virtual python environment with the `venv` package. For this the `ExecutionPolicy` must be set to `RemoteSigned`.

```powershell
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned
```

**This has been done on the computer which is setup for the Superresolution Research Practicum!**

Now you can login with your account and open `PowerShell`. In this shell you will install the directory for you research practicum `rp` in your home directory. This installation is done by *cloning* the *git repository* in your home directory.

```powershell
cd $HOME
git clone https://gitlab.tudelft.nl/education/as/ap/rp_superresolution.git rp
cd rp
```

`cd` will change the working directory to `$HOME` which is a environment variable set to the path of your home-directory. `git clone ...` will create a directory with the name `rp` and clone the git repository in that directory. `cd` will then change the working directory to this newly created `rp` directory in you home-directory.

Create a virtual python environment in the `env` directory and install all packages from this file in ths virtual environment:

```powershell
python -m venv env
.\env\Scripts\Activate.ps1
pip install -r requirements.txt --upgrade
```

`python` is the actual Python program; the `-m venv` option tells Python to run the `venv` package with the option `env` which is the name of the directory containing the virtual environment. Next you start the virtual environment with `.\env\Scripts\Activate.ps1` which contains some instructions to tell Windows which Python should be used and tell Python where the packages are installed.

*Note: once you've activated the virtual environment you will see the name of the directory of this environment between brackets before the prompt (`<netid>` will be your username):*

```powershell
(env) PS C:\Users\<netid>\rp>
```

### Create Desktop Shortcut

You don't need to create a desktop shortcut to start `Jupyter Notebook` as you can easily start this via the command-line. Make sure you activated the virtual environment (check the `(env)` before the prompt):

```powershell
jupyter notebook
```

But if you really like clicking stuff...

Copy the `Jupyter notebook.lnk` shortcut to your desktop or **Start Menu**. You can find the location of the **Start Menu** with **File Explorer**. Go to `%AppData%\Microsoft\Windows\Start Menu\Programs`.

#### Do it yourself

If you feel adventurous you can create a Desktop Shortcut yourself to start `Jupyter notebook`:

1. right-click on the desktop and select **New->Shortcut**
1. go to the **Shortcut** tab and fill in the following values:
    * **Target**: `%USERPROFILE%\rp\env\Scripts\jupyter notebook`
    * **Start in**: `%USERPROFILE%\rp`
    * **Run**: Minimized
    * click **Change Icon...** and **Browse**, select `jupyter_app_icon_512px`, click OK
1. click **OK**

## Normal usage

These instructions are for day-to-day usage.

Once you've logged in with your account you open `PowerShell` and start `Jupyter notebook` from the virtual environment (or click the Desktop Shortcut...):

```powershell
cd $HOME\rp
.\env\Scripts\Activate.ps1
jupyter notebook
```

This will open up a browser with the Jupyter Notebook web-application running from your `rp` directory.

When you're finished working in Jupyter Notebook select `Shutdown` from the menu. You can now deactivate the virtual environment in the `PowerShell`:

```powershell
deactivate
```

And close `PowerShell`.

## Upgrade python and packages

Python can only be upgraded by the administrator. If upgraded you will be notified to upgrade your virtual environment:

```powershell
cd $HOME\rp
python -m venv env
```

All packages in your virtual environment can be upgraded by yourself. You can check if packages are outdated:

```powershell
cd $HOME\rp
.\env\Scripts\Activate.ps1
pip list --outdated
```

And upgrade:

```powershell
pip install -r requirements.txt --upgrade
```
